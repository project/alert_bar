<?php

/**
 * @file
 * Contains \Drupal\alert_bar\Form\AlertBarModuleSettingsForm
 */
namespace Drupal\alert_bar\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Configure alert_bar settings for this site.
 */
class AlertBarModuleSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'alert_bar_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'alert_bar.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('alert_bar.module_settings');

    $form['alert_bar_toggle_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Toggle Settings'),
      '#open' => TRUE,
    ];

    $form['alert_bar_toggle_settings']['alert_bar_toggle_severity'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable severity selection.'),
      '#description' => $this->t('When checked, the "Severity" select box on the <em>Manage alert bar</em> page will not be available.'),
      '#default_value' => $config->get('hide_severity'),
    ];

    $form['alert_bar_toggle_settings']['alert_bar_toggle_additional_settings'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable the Additional Settings configuration.'),
      '#description' => $this->t('When checked, the "Additional Settings" section of the <em>Manage alert bar</em> page will not be available.'),
      '#default_value' => $config->get('hide_additional_settings'),
    ];

    $form['alert_bar_toggle_settings']['alert_bar_toggle_more_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable the "More link".'),
      '#description' => $this->t('When checked, the "More link" section of the <em>Manage alert bar</em> page will not be available.'),
      '#default_value' => $config->get('hide_more_link'),
    ];

    $form['alert_bar_toggle_settings']['alert_bar_toggle_plain_text'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Limit the Message field to plain text.'),
      '#description' => $this->t('When checked, the "Message" field of the <em>Manage alert bar</em> page will only allow plain text.'),
      '#default_value' => $config->get('plain_text_only'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = \Drupal::service('config.factory')->getEditable('alert_bar.module_settings');
    $config
      ->set('hide_severity', $form_state->getValue('alert_bar_toggle_severity'))
      ->set('hide_additional_settings', $form_state->getValue('alert_bar_toggle_additional_settings'))
      ->set('hide_more_link', $form_state->getValue('alert_bar_toggle_more_link'))
      ->set('plain_text_only', $form_state->getValue('alert_bar_toggle_plain_text'))
      ->save();

    // Nullify the alert settings that are hidden.
    $config = \Drupal::service('config.factory')->getEditable('alert_bar.settings');

    if ($form_state->getValue('alert_bar_toggle_severity')) {
      $config->set('severity', NULL)->save();
    }

    if ($form_state->getValue('alert_bar_toggle_additional_settings')) {
      $config
        ->set('alert_color_scheme', NULL)
        ->set('additional_classes', NULL)
        ->save();
    }

    Cache::invalidateTags(array('config:alert_bar.module_settings'));

  }
}
