<?php

/**
 * @file
 * Contains \Drupal\alert_bar\Form\AlertBarSettingsForm
 */

namespace Drupal\alert_bar\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Configure alert_bar settings for this site.
 */
class AlertBarSettingsForm extends ConfigFormBase
{

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'alert_bar_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      'alert_bar.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {

    $config = $this->config('alert_bar.settings');
    $module_config = $this->config('alert_bar.module_settings');

    $form['alert_bar_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the alert message.'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['alert_bar_homepage_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Limit the alert to the homepage.'),
      '#default_value' => $config->get('homepage_only'),
    ];

    $form['alert_bar_expires'] = [
      '#type' => 'date',
      '#title' => $this->t('Expiration'),
      '#default_value' => $config->get('expires'),
      '#description' => $this->t(/** @lang text */ "The expiration date is the last day the alert will be displayed.<br>If no expiration is set, the alert will continue to display until \"@enabled_title\" is unchecked.", array(
        '@enabled_title' => $form['alert_bar_enabled']['#title'],
      )),
    ];

    if ($module_config->get('hide_severity') !== 1) {

      $severity = $config->get('severity');
      $form['alert_bar_severity'] = [
        '#type' => 'select',
        '#title' => $this->t('Severity'),
        '#required' => true,
        '#options' => [
          'message' => $this->t('Message'),
          'warning' => $this->t('Warning'),
          'critical' => $this->t('Critical'),
        ],
        '#default_value' => (($severity) ? $severity : 'message'),
      ];

    }

    $message = $config->get('message');
    if ($module_config->get('plain_text_only') !== 1) {
      $form['alert_bar_message'] = [
        '#type' => 'text_format',
        '#title' => $this->t('Message'),
        '#required' => true,
        '#format' => $message['format'] ?? NULL,
        '#default_value' => $message['value'] ?? NULL,
      ];
    } else {
      $form['alert_bar_message'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Message'),
        '#required' => true,
        '#default_value' => $message['value'] ?? NULL,
        '#rows' => 5,
      ];
    }

    if ($module_config->get('hide_more_link') !== 1) {

      $form['alert_bar_more_link'] = [
        '#type' => 'details',
        '#title' => $this->t('More link'),
        '#open' => TRUE,
      ];

      $form['alert_bar_more_link']['alert_bar_more_link_label'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Link label'),
        '#placeholder' => $this->t('Link label'),
        '#description' => $this->t('If a URL is provided and this field is left blank, the label will default to "Learn More".'),
        '#default_value' => $config->get('more_link_label'),
      ];

      $form['alert_bar_more_link']['alert_bar_more_link_url'] = [
        '#type' => 'url',
        '#title' => $this->t('Link URL'),
        '#placeholder' => $this->t('Link URL'),
        '#description' => $this->t(/** @lang html */ 'If this field is left blank, the link will not appear. <b>Note:</b> Only full URLs are accepted.'),
        '#maxlength' => 512,
        '#default_value' => $config->get('more_link_url'),
      ];

      $form['alert_bar_more_link']['alert_bar_more_link_external'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Open link in a new browser.'),
        '#default_value' => $config->get('more_link_external'),
      ];
    }

    if ($module_config->get('hide_additional_settings') !== 1) {
      $form['alert_bar_alert_settings'] = [
        '#type' => 'details',
        '#title' => $this->t('Additional Settings'),
        '#open' => TRUE,
      ];

      $color_scheme = $config->get('alert_color_scheme');
      $form['alert_bar_alert_settings']['alert_bar_alert_color_scheme'] = [
        '#type' => 'select',
        '#title' => $this->t('Color scheme'),
        '#required' => true,
        '#options' => [
          'scheme-default' => $this->t('Default'),
          'scheme-alternate' => $this->t('Alternate'),
        ],
        '#description' => $this->t('Select between "Default" and "Alternate" color schemes for the alert.'),
        '#default_value' => (($color_scheme) ? $color_scheme : 'alternate'),
      ];

      $form['alert_bar_alert_settings']['alert_bar_additional_classes'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Additional classes'),
        '#description' => $this->t('Provide additional CSS classes to the alert. Separate multiple classes with spaces.'),
        '#default_value' => $config->get('additional_classes'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    parent::submitForm($form, $form_state);

    // $config = $this->config('alert_bar.settings');
    $config = \Drupal::service('config.factory')->getEditable('alert_bar.settings');
    $config
      ->set('enabled', $form_state->getValue('alert_bar_enabled'))
      ->set('expires', $form_state->getValue('alert_bar_expires'))
      ->set('severity', $form_state->getValue('alert_bar_severity'))
      ->set('homepage_only', $form_state->getValue('alert_bar_homepage_only'))
      ->set('more_link_label', $form_state->getValue('alert_bar_more_link_label'))
      ->set('more_link_url', $form_state->getValue('alert_bar_more_link_url'))
      ->set('more_link_external', $form_state->getValue('alert_bar_more_link_external'))
      ->set('alert_color_scheme', $form_state->getValue('alert_bar_alert_color_scheme'))
      ->set('additional_classes', $form_state->getValue('alert_bar_additional_classes'))
      ->set('unique_id', uniqid());

    $message = $form_state->getValue('alert_bar_message');
    if (!is_array($message)) {
      $message = [
        'value' => $message,
        'format' => null,
      ];
    }

    $config->set('message', $message);

    $config->save();

    Cache::invalidateTags(array('config:alert_bar.settings'));

  }
}
