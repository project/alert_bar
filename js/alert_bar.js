(function ($, Drupal, cookies) {
  Drupal.behaviors.alertBar = {
    attach: function (context, settings) {

      var cookieintro = 'alert_bar_closed_';
      var alert_bar_closed = cookies.get(cookieintro.concat(settings.alert_bar.unique_id), Number);
      var is_root = (location.pathname === "/");
      if (drupalSettings.alertSettings.homepage_only === 1 && is_root !== true) {
        alert_bar_closed = 1;
      }

      if (!alert_bar_closed) {
        if (drupalSettings.alertSettings.enabled) {
          if (drupalSettings.alertSettings.expires === null) {
            $('#alert-bar').addClass('show-alert');
          } else {
            var current_time = Math.round(new Date().getTime() / 1000);
            if (current_time < drupalSettings.alertSettings.expires) {
              $('#alert-bar').addClass('show-alert');
            }
          }
        }
      }

      $('.alert-bar-close', context).on('click', function (e) {
        e.preventDefault();
        let alert_bar = $('#alert-bar');
        if (alert_bar.length > 0) {
          alert_bar.removeClass('show-alert');
          cookies.set(cookieintro.concat(settings.alert_bar.unique_id), 1);
        }
      });

    }
  };
})(jQuery, Drupal, window.Cookies);
